//
//  ViewController.swift
//  ModelIOApp
//
//  Created by idz on 5/9/16.
//  Copyright © 2016 iOS Developer Zone.
//  License: MIT
//  See: https://raw.githubusercontent.com/iosdevzone/GettingStartedWithModelIO/master/LICENSE
//

import UIKit
import ModelIO
import SceneKit
import SceneKit.ModelIO
import Zip

extension MDLMaterial {
    func setTextureProperties(_ textures: [MDLMaterialSemantic:String]) -> Void {
        for (key,value) in textures {
            guard let url = Bundle.main.url(forResource: value, withExtension: "png") else {
                fatalError("Failed to find URL for resource \(value).")
            }
            let property = MDLMaterialProperty(name:value, semantic: key, url: url)
            self.setProperty(property)
        }
    }
    
    func setTexturePropertiesWithURLPath(_ textures: [MDLMaterialSemantic:String], urlPath: URL) -> Void {
        for (key,value) in textures {
//            guard let url = Bundle.main.url(forResource: value, withExtension: "") else {
//                fatalError("Failed to find URL for resource \(value).")
//            }
            let image = UIImage.init(contentsOfFile: "")
            
            let property = MDLMaterialProperty(name:value, semantic: key, url: urlPath)
            self.setProperty(property)
        }
    }
}


class ViewController: UIViewController {
    
    var sceneView: SCNView {
        return self.view as! SCNView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        unzipFiles()
    }
    
    func unzipFiles() {
        do {
            let filePath = Bundle.main.url(forResource: "Models", withExtension: "zip")!
            let unzipDirectory = try Zip.quickUnzipFile(filePath)
            setupScene(filePath: unzipDirectory.absoluteString)
//            setupSceneWithModelIO(filePath: unzipDirectory.absoluteString)
        } catch  {
            
        }
    }
    
    func setupSceneWithModelIO(filePath: String) {
        let fileName =  "firetruck.obj"
//        let url = URL.init(string: filePath + fileName)!
        
        let searchPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentpath = searchPaths.first
        let pathURL = URL.init(string: documentpath! + "/Models/Models/")!
        let docURL = URL.init(string: documentpath! + "/Models/Models/" + fileName)!
        
        let asset = MDLAsset.init(url: docURL)
        guard let object = asset.object(at: 0) as? MDLMesh else {
            fatalError("Failed")
        }
        
        
        // Apply the texture to every submesh of the asset
        for submesh in object.submeshes! {
            let scatteringFunction = MDLScatteringFunction()
            if let submesh = submesh as? MDLSubmesh, submesh.material?.name == "materialBody" {
                let materialBody = MDLMaterial(name: "materialBody", scatteringFunction: scatteringFunction)
//                materialBody.setTextureProperties([MDLMaterialSemantic.baseColor:""])
                materialBody.setTexturePropertiesWithURLPath([MDLMaterialSemantic.baseColor : "BodyBaseColor.png"], urlPath: URL.init(string: pathURL.absoluteString + "BodyBaseColor.png")!)
                submesh.material = materialBody
            }
            
            if let submesh = submesh as? MDLSubmesh, submesh.material?.name == "materialAccessories" {
                let materialAccessories = MDLMaterial(name: "materialAccessories", scatteringFunction: scatteringFunction)
//                materialAccessories.setTextureProperties([MDLMaterialSemantic.baseColor:"AccessoriesBaseColor"])
                materialAccessories.setTexturePropertiesWithURLPath([MDLMaterialSemantic.baseColor : "AccessoriesBaseColor"], urlPath: URL.init(string: pathURL.absoluteString + "AccessoriesBaseColor.png")!)

                submesh.material = materialAccessories
            }
        }
 
        
        sceneView.autoenablesDefaultLighting = true
        sceneView.allowsCameraControl = true
        sceneView.backgroundColor = UIColor.darkGray
        
        let node = SCNNode.init(mdlObject: object)
        let scene = SCNScene()
        scene.rootNode.addChildNode(node)
        sceneView.scene = scene
    }
    
    func setupScene(filePath: String) {
        sceneView.allowsCameraControl = true
        sceneView.backgroundColor = UIColor.darkGray
        sceneView.autoenablesDefaultLighting = true
        
        let fileName =  "firetruck.obj"
        let url = URL.init(string: filePath + "Models/" + fileName)!
        
        let searchPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentpath = searchPaths.first
        let pathURL = URL.init(string: documentpath! + "/Models/Models/")!
        let docURL = URL.init(string: documentpath! + "/Models/Models/" + fileName)!
        
        let asset = MDLAsset.init(url: docURL)
        
//        let anUlr = Bundle.main.url(forResource: "firetruck", withExtension: ".obj")
//        let scene = SCNScene.init(mdlAsset: asset)
        
        let sceneSource = SCNSceneSource.init(url: docURL, options: [SCNSceneSource.LoadingOption.assetDirectoryURLs: [pathURL, NSURL.init(string: filePath), NSURL.init(string: filePath + "Models/")], SCNSceneSource.LoadingOption.overrideAssetURLs: true])
        
        let scene = SCNScene.init()
//        let scene = try! SCNScene.init(url: docURL, options: [SCNSceneSource.LoadingOption.assetDirectoryURLs: [pathURL, NSURL.init(string: filePath), NSURL.init(string: filePath + "Models/")], SCNSceneSource.LoadingOption.overrideAssetURLs: true])
//        let scene = try! SCNScene.init(url: URL.init(fileURLWithPath: docURL.absoluteString), options: nil)
        
        let sourceNode = sceneSource?.entryWithIdentifier("MDL_Obj2.001_materialAccessories", withClass: SCNNode.self)
//        sceneView.scene = scene
        sceneView.scene = sceneSource?.scene(options: [SCNSceneSource.LoadingOption.assetDirectoryURLs: [pathURL, NSURL.init(string: filePath), NSURL.init(string: filePath + "Models/")], SCNSceneSource.LoadingOption.overrideAssetURLs: true])
        
        
//        scene.rootNode.addChildNode(sourceNode!)
        
        // Set up the SceneView
        let lNode = SCNNode()
        lNode.light = SCNLight()
        lNode.light?.type = .omni
//        lNode.castsShadow = true
        lNode.position = SCNVector3.init(0, -10000, 0)
        lNode.light?.color = UIColor.white.cgColor
        
        let anotherNode = SCNNode()
        anotherNode.light = SCNLight()
        anotherNode.light?.type = .ambient
        
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3.init(0, 10, 0)
        
//        scene?.rootNode.addChildNode(cameraNode)
//        scene?.rootNode.addChildNode(anotherNode)
//
        
//
//        scene?.rootNode.addChildNode(lNode)
        
//        scene?.rootNode.position = SCNVector3.init(0, 0, 0)
        print("Loaded.")
    }
    
    func preFunction() {
        // Load the .OBJ file
        guard let url = Bundle.main.url(forResource: "GT46AC_Frame", withExtension: "obj") else {
            fatalError("Failed to find model file.")
        }
        
        let asset = MDLAsset(url:url)
        guard let object = asset.object(at: 0) as? MDLMesh else {
            fatalError("Failed to get mesh from asset.")
        }
        
        // Create a material from the various textures
        //        let scatteringFunction = MDLScatteringFunction()
        //        let accessoryMDL = MDLMaterial(name: "accessoryMDL", scatteringFunction: scatteringFunction)
        //        let bodyMDL = MDLMaterial(name: "bodyMDL", scatteringFunction: scatteringFunction)
        //        bodyMDL.setTextureProperties([
        //            .baseColor:"Fighter_Diffuse_25.jpg",
        //            .specular:"Fighter_Specular_25.jpg",
        //            .emission:"Fighter_Illumination_25.jpg"])
        //
        //        // Apply the texture to every submesh of the asset
        for  submesh in object.submeshes!  {
            if let submesh = submesh as? MDLSubmesh {
                //                submesh.material = bodyMDL
            }
        }
        
        
        
        // Apply the materials to SceneKit
        //        let bodyMaterial = SCNMaterial(mdlMaterial: bodyMDL)
        //        bodyMaterial.lightingModel = .blinn
        //        bodyMaterial.diffuse.contents = UIColor.yellow
        //        bodyMaterial.reflective.contents = UIColor.gray
        //        bodyMaterial.normal.contents = UIColor.red
        //        bodyMaterial.normal.contents = UIImage.init(named: "BodyNormalMap2")
        //        bodyMaterial.ambientOcclusion.contents = UIColor.blue
        
        //        let accessoryMaterial = SCNMaterial(mdlMaterial: accessoryMDL)
        //        accessoryMaterial.name = "accessoryMaterial"
        //        accessoryMaterial.diffuse.contents = UIImage.init(named: "Fighter_Specular_25.jpg")
        //        accessoryMaterial.lightingModel = .blinn
        //        accessoryMaterial.normal.contents = UIImage.init(named: "AccessoriesNormalMap2")
        //        accessoryMaterial.reflective.contents = UIColor.black
        //        accessoryMaterial.transparent.contents = UIColor.white
        //        accessoryMaterial.ambientOcclusion.contents = UIImage.init(named: "AccessoriesAOMap2")
        //        accessoryMaterial.selfIllumination.contents = UIColor.black
        //        accessoryMaterial.emission.contents = UIColor.black
        //        accessoryMaterial.multiply.contents = UIColor.white
        //        accessoryMaterial.ambient.contents = UIColor.black
        //        accessoryMaterial.displacement.contents = UIColor.black
        
        let geometry = SCNGeometry(mdlMesh: object)
        //        var i = 0
        //        for _ in geometry.materials {
        //            geometry.replaceMaterial(at: i, with: bodyMaterial)
        //            i += 1
        //        }
        //        geometry.materials = [bodyMaterial, accessoryMaterial, accessoryMaterial, accessoryMaterial]
        
        // Wrap the ModelIO object in a SceneKit object
        //        let node = SCNNode(mdlObject: object)
        //        let light1 = SCNLight.init()
        //        light1.type = .spot
        
        //        let node = SCNNode(geometry: geometry)
        //        node.scale = SCNVector3Make(0.1, 0.1, 0.1)
        //        node.light = light1
        //        for mat in (node.geometry?.materials)! {
        //            mat.diffuse.contents = UIColor.gray
        //            mat.lightingModel = .blinn
        //        }
        
        //        let scene = SCNScene()
        //        let scene = SCNScene(mdlAsset: asset)
        let scene = SCNScene(named: "GT46AC_Frame.obj")
        //        scene.rootNode.addChildNode(node)
        let node = scene?.rootNode.childNodes.first
        for obj in (node?.geometry?.materials)! {
            obj.lightingModel = .blinn
            obj.diffuse.contents = UIColor.gray
            obj.ambient.contents = UIColor.white
            obj.specular.contents = UIColor.white
            obj.emission.contents = UIColor.red
        }
        
        let cameraNode = SCNNode()
        //        cameraNode.camera = SCNCamera()
        //        cameraNode.position = SCNVector3.init(30, 30, -30)
        cameraNode.light = SCNLight()
        cameraNode.light?.type = .omni
        cameraNode.position = SCNVector3.init(4079, -10905, -1441)
        cameraNode.light?.color = UIColor.white.cgColor
        scene?.rootNode.addChildNode(cameraNode)
        
        // Set up the SceneView
        sceneView.autoenablesDefaultLighting = true
        sceneView.allowsCameraControl = true
        sceneView.scene = scene
        sceneView.backgroundColor = UIColor.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

